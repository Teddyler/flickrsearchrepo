//
//  SearchCollectionViewCell.swift
//  FlickrSearch
//
//  Created by Tyler Barnes on 3/1/17.
//  Copyright © 2017 Tyler Barnes. All rights reserved.
//

import UIKit

class SearchCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var flickrImage: UIImageView!
    @IBOutlet var imageDesc: UILabel!
    var cellNum:Int? = nil
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
}
