//
//  SearchViewController.swift
//  FlickrSearch
//
//  Created by Tyler Barnes on 3/1/17.
//  Copyright © 2017 Tyler Barnes. All rights reserved.
//

import UIKit

class SearchViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    @IBOutlet var searchResultsCollectionView: UICollectionView!
    
    let apiKey = "1dd17dde0fed7286935d83875fcc17dd"
    
    public var searchTerm: String?
    public var photos: NSDictionary? = nil
    public var photoList: [NSDictionary] = []
    public var photoID : String? = nil
    public var farm : Int? = nil
    public var server : String? = nil
    public var secret : String? = nil
    public var photoTitle: String? = nil
    public var image: UIImage? = nil
    var item: Int = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        searchResultsCollectionView.register(UINib(nibName: "SearchCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "cell")
        
        accessFlickr()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        numberOfItemsInSection section: Int) -> Int {
            return photoList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! SearchCollectionViewCell
        
        let object = self.photoList[indexPath.row] as! [String : Any]
        
        cell.imageDesc.textColor = UIColor.green
        cell.imageDesc.text = "changed"
        
//        photoID = photoList[item]["id"] as? String
//        farm = photoList[item]["farm"] as? Int
//        server = photoList[item]["server"] as? String
//        secret = photoList[item]["secret"] as? String
//        photoTitle = photoList[item]["title"] as? String
        
//        if(item < photoList.count){
//            let queue = DispatchQueue(label: "queue")
//            queue.sync{
                self.loadImage(object: object , cell: cell)
//            }
//            item += 1
//        }
        
//        cell.flickrImage.image = self.image
        cell.imageDesc.text = object["title"] as! String?
        
        // Configure the cell
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: 202, height: 202)
    
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        performSegue(withIdentifier: "detail", sender: indexPath)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        let indexPath = searchResultsCollectionView.cellForItem(at: sender as! IndexPath) as? SearchCollectionViewCell
        let rvc = segue.destination as! ResultsViewController
        rvc.img = indexPath?.flickrImage.image
        rvc.desc = (indexPath?.imageDesc.text)!
    }
    
    func accessFlickr(){
        
        let url = flickrSearchURLForSearchTerm(searchTerm!)
        
        let task = URLSession.shared.dataTask(with: url!) { data, response, error in
            
            guard error == nil else {
                print(error!)
                return
            }
            guard let data = data else {
                print("Data is empty")
                return
            }
            
            let json = try! JSONSerialization.jsonObject(with: data, options: []) as? NSDictionary

            self.photos = json?["photos"] as? NSDictionary
            self.photoList = self.photos?["photo"] as! [NSDictionary]
            
            DispatchQueue.main.sync(execute: {self.searchResultsCollectionView.reloadData()});
        }
        
        task.resume()
        
    }
    
    func loadImage (object: [String:Any], cell: SearchCollectionViewCell){
        
        photoID = object["id"] as? String
        farm = object["farm"] as? Int
        server = object["server"] as? String
        secret = object["secret"] as? String
        photoTitle = object["title"] as? String
        
        let URLString = "https://farm\(farm!).staticflickr.com/\(server!)/\(photoID!)_\(secret!).jpg"
        
        guard let imageUrl = URL(string:URLString) else {
            print("URL Failed")
            return
        }
        
        print("Here")
        
        let session = URLSession.shared.dataTask(with: imageUrl) { (data, response, error) in
            if error != nil {
                print("Image Load Failed")
                return
            }
            else {
                DispatchQueue.main.sync(execute: {
                    cell.flickrImage.image =  UIImage(data: data!)
                });
                
            }

        }
        
        session.resume()
    }
    
    func flickrSearchURLForSearchTerm(_ searchTerm:String) -> URL? {
        
        guard let escapedTerm = searchTerm.addingPercentEncoding(withAllowedCharacters: CharacterSet.alphanumerics) else {
            return nil
        }
        
        let URLString = "https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=\(apiKey)&text=\(escapedTerm)&per_page=25&format=json&nojsoncallback=1"
        
        guard let url = URL(string:URLString) else {
            return nil
        }
        
        return url
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
