//
//  ViewController.swift
//  FlickrSearch
//
//  Created by Tyler Barnes on 3/1/17.
//  Copyright © 2017 Tyler Barnes. All rights reserved.
//

import UIKit
import CoreData

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {

    @IBOutlet var oldSearchTable: UITableView!
    @IBOutlet var searchBar: UISearchBar!
    
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext

    var searchArray: [String]?;
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        oldSearchTable.delegate = self
        oldSearchTable.dataSource = self
        
        self.navigationItem.rightBarButtonItem = self.editButtonItem
        
        loadData()
    }
    
    override func loadView() {
        super.loadView()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        loadData()
        oldSearchTable.reloadData()
    }
    
    func loadData (){
    
    
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        
        let request = Searches(context: context)
        
        request.search = "Puppies"
        
        (UIApplication.shared.delegate as! AppDelegate).saveContext()
        
        do {
            searchArray = try context.fetch(Searches.fetchRequest()) as? [String]
        } catch {
            print("Fetching Failed")
        }
        
        oldSearchTable.reloadData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if !(searchArray?.isEmpty == true){
            return 0;
        }
        else {
            return (searchArray?.count)!
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = oldSearchTable.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        
        cell.textLabel?.text = searchArray![indexPath.row]
        
        return cell
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        
        let request = Searches(context: context)
        
        request.search = searchBar.text
        
        (UIApplication.shared.delegate as! AppDelegate).saveContext()
        
        performSegue(withIdentifier: "search", sender: Any?.self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "search"){
            let svc = segue.destination as! SearchViewController
            svc.searchTerm = searchBar.text
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

