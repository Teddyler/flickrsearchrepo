//
//  ResultsViewController.swift
//  FlickrSearch
//
//  Created by Tyler Barnes on 3/3/17.
//  Copyright © 2017 Tyler Barnes. All rights reserved.
//

import UIKit

class ResultsViewController: UIViewController {

    @IBOutlet var imageDesc: UILabel!
    @IBOutlet var image: UIImageView!
    
    public var desc: String = ""
    public var img: UIImage? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        imageDesc.text = desc
        image.image = img
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
